(function() {
    'use strict';

    angular
        .module('shinypocket.login')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['$window', '$state', 'logger', 'coreFactory', 'authService'];

    function LoginController($window, $state, logger, coreFactory, authService) {

        var vm   = this;

        vm.user  = {};

        vm.login = login;

        ////////// Controller functions //////////

        function login() {
            coreFactory.authenticate.save(vm.user, function(result) {

                authService.storeUserCredentials(result.data.token);
                authService.loadUserCredentials();
                $window.location.reload(true);
                //$state.go('app.home', {}, {reload: true});
            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

    }
})();