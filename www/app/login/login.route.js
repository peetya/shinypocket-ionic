(function() {
    'use strict';

    angular
        .module('shinypocket.login')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.login', {
                url: "/login",
                views: {
                    'menuContent': { templateUrl: "app/login/login.template.html", controller: "LoginController as vm" }
                }
            })

    }

})();