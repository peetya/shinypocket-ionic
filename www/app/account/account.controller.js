(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .controller('AccountController', AccountController);

    AccountController.$inject = ['$q', '$scope', '$state', '$ionicModal', 'logger', 'accountService', 'iconService'];

    function AccountController($q, $scope, $state, $ionicModal, logger, accountService, iconService) {

        var vm = this;

        vm.accounts   = {};
        vm.pagination = {};
        vm.icons      = iconService.icons;

        vm.openAccountDeleteModal = openDeleteModal;
        vm.openAccountCreateModal = openCreateModal;
        vm.openAccountEditModal   = openEditModal;
        vm.closeEditModal         = closeEditModal;
        vm.closeCreateModal       = closeCreateModal;
        vm.closeDeleteModal       = closeDeleteModal;

        vm.edit                   = edit;
        vm.create                 = create;
        vm.deletee                = deletee;

        activate();

        function activate() {

            $ionicModal.fromTemplateUrl('app/account/account.delete.modal.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.deleteModal = modal;
            });

            $ionicModal.fromTemplateUrl('app/account/account.new.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.createModal = modal;
            });

            $ionicModal.fromTemplateUrl('app/account/account.edit.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.editModal = modal;
            });

            $scope.$on('$destroy', function() {
                vm.deleteModal.remove();
                vm.createModal.remove();
                vm.editModal.remove();
            });

            var promises = [getAccounts()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('app.home');

            });

        }

        ////////// Controller functions //////////

        function getAccounts() {
            var deferrer = $q.defer();

            accountService.accounts().get(function(accounts) {

                deferrer.resolve();
                vm.accounts   = accounts.data;
                vm.pagination = accounts.pagination;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function openDeleteModal(accountId) {

            vm.accountId = accountId;
            $scope.deleteModal.show();

        }

        function openCreateModal() {

            $scope.createModal.show();

        }

        function openEditModal(accountId) {

            accountService.accounts().get({id: accountId}, function(account) {

                vm.account   = account.data;
                vm.accountId = accountId;
                $scope.editModal.show();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

        function closeEditModal() {

            $scope.editModal.hide();
            vm.account   = {};
            vm.accountId = '';
        }

        function closeCreateModal() {

            $scope.createModal.hide();
            vm.account   = {};
            vm.accountId = '';

        }

        function closeDeleteModal() {

            $scope.deleteModal.hide();
            vm.accountId = '';

        }

        function edit() {

            var data = {
                "icon": vm.account.icon,
                "name": vm.account.name
            };
            accountService.accounts().update({id: vm.accountId}, data, function(account) {

                logger.success('Account successfully modified!');
                $state.go('app.account', {}, {reload: true});
                closeEditModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

        function create() {

            accountService.accounts().save(vm.account, function(account) {

                logger.success('Account successfully created!');
                $state.go('app.account', {}, {reload: true});
                closeCreateModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

        function deletee() {

            accountService.accounts().remove({id: vm.accountId}, function(account) {

                logger.success('Account successfully removed!');
                $state.go('app.account', {}, {reload: true});
                closeDeleteModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

    }
})();