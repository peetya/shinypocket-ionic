(function() {
    'use strict';

    angular
        .module('shinypocket.account')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.account', {
                url: "/my-accounts",
                authenticate: true,
                views: {
                    'menuContent': { templateUrl: "app/account/account.template.html", controller: "AccountController as vm" }
                }
            })

            .state('app.account.new', {
                url: "/new",
                authenticate: true,
                views: {
                    'menuContent@': { templateUrl: "app/account/account.new.template.html", controller: "AccountNewController as vm" }
                }
            })

            .state('app.account.edit', {
                url: "/edit/:id",
                authenticate: true,
                views: {
                    'menuContent@': { templateUrl: "app/account/account.edit.template.html", controller: "AccountEditController as vm" }
                }
            })

    }

})();