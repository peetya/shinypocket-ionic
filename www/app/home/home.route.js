(function() {
    'use strict';

    angular
        .module('shinypocket.home')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.home', {
                url: "/",
                views: {
                    'menuContent': { templateUrl: "app/home/home.template.html", controller: "HomeController as vm" }
                }
            })

    }

})();