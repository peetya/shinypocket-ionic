(function() {
    'use strict';

    angular
        .module('shinypocket.signup')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.signup', {
                url: "/signup",
                views: {
                    'menuContent': { templateUrl: "app/signup/signup.template.html", controller: "SignupController as vm" }
                }
            })

    }

})();