(function() {
    'use strict';

    angular
        .module('shinypocket.signup')
        .controller('SignupController', SignupController);

    SignupController.$inject = ['$window', '$state', 'logger', 'coreFactory', 'authService'];

    function SignupController($window, $state, logger, coreFactory, authService) {

        var vm   = this;

        vm.user  = {};

        vm.signup = signup;

        ////////// Controller functions //////////

        function signup() {
            if (vm.user.password == vm.user.password2) {
                coreFactory.register.save(vm.user, function(result) {

                    logger.success("Successful registration! Now you can log in with your account.");
                    $state.go('app.home');

                }, function(error) {

                    logger.error(error.data.data.message);

                });
            } else {
                logger.error("The given passwords are not the same. Please try it again!");
            }

        }

    }
})();