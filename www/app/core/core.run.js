(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .run(appRun);

    appRun.$inject = ['$rootScope', '$ionicPlatform', '$state', '$uibModalStack', 'authService', 'amMoment', 'logger'];

    function appRun($rootScope, $ionicPlatform, $state, $uibModalStack, authService, amMoment, logger) {

        var vm = this;

        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeStart', function(event, next, nextParams, fromState) {

            $uibModalStack.dismissAll();

            if (!authService.isAuthenticated()) {

                if (next.authenticate == true) {
                    event.preventDefault();
                    $state.go('app.login');
                    logger.warning("Access denied. Please log in.");
                }

            } else {

                if (next.name == 'app.login' || next.name == 'app.signup') {
                    event.preventDefault();
                    $state.go('app.home');
                }

            }

        });

    }
})();