(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .config(appConfig);

    function appConfig($httpProvider, toastrConfig) {

        $httpProvider.interceptors.push("AuthInterceptor");

        angular.extend(toastrConfig, {
            positionClass: 'toast-bottom-full-width'
        });

    }

})();