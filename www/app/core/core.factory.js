(function () {
    'use strict';

    angular
        .module('shinypocket.core')
        .factory('coreFactory', coreFactory);

    function coreFactory($resource) {

        return {
            currentUser: currentUser(),
            authenticate: authenticateUser(),
            register: registerUser()
        };

        //////////////////////////
        // Services
        //////////////////////////
        function currentUser() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/users/me', {}, {});
        }

        function authenticateUser() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/auth/authenticate', {}, {});
        }

        function registerUser() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/auth/register', {}, {});
        }

    }

})();