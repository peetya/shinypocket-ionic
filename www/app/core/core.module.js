(function() {
    'use strict';

    angular.module('shinypocket.core', [
        /**
         * Angular modules
         */
        'ngResource',
        'ngAnimate',
        'angular-jwt',
        'angularMoment',
        'ui.router',
        'ui.bootstrap',
        'toastr',
        'nvd3',
        'ionic',

        /**
         * Reusable cross app code modules
         */
        'shinypocket.logger'

        /**
         * 3rd Party modules
         */
    ]);
})();