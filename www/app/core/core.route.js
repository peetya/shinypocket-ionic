(function() {
    'use strict';

    angular
        .module('shinypocket.core')
        .config(stateConfig);

    stateConfig.$inject = ['$urlRouterProvider', '$locationProvider', '$stateProvider'];

    function stateConfig($urlRouterProvider, $locationProvider, $stateProvider) {

        $urlRouterProvider.otherwise("/app/");
        //$locationProvider.html5Mode(true);

        $stateProvider
            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'app/core/_layout/menu.template.html',
                controller: 'MainController as mainVm'
            });

    }

})();