(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.category', {
                url: "/categories",
                authenticate: true,
                views: {
                    'menuContent': { templateUrl: "app/category/category.template.html", controller: "CategoryController as vm" }
                }
            })

            .state('app.category.new', {
                url: "/new",
                authenticate: true,
                views: {
                    'menuContent@': { templateUrl: "app/category/category.new.template.html", controller: "CategoryNewController as vm" }
                }
            })

            .state('app.category.edit', {
                url: "/edit/:id",
                authenticate: true,
                views: {
                    'menuContent@': { templateUrl: "app/category/category.edit.template.html", controller: "CategoryEditController as vm" }
                }
            })

    }

})();