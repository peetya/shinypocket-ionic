(function() {
    'use strict';

    angular
        .module('shinypocket.category')
        .controller('CategoryController', CategoryController);

    CategoryController.$inject = ['$q', '$scope', '$state', '$ionicModal', 'logger', 'categoryService', 'iconService'];

    function CategoryController($q, $scope, $state, $ionicModal, logger, categoryService, iconService) {

        var vm = this;

        vm.categories   = {};
        vm.pagination = {};
        vm.icons      = iconService.icons;

        vm.openCategoryDeleteModal = openDeleteModal;
        vm.openCategoryCreateModal = openCreateModal;
        vm.openCategoryEditModal   = openEditModal;
        vm.closeEditModal          = closeEditModal;
        vm.closeCreateModal        = closeCreateModal;
        vm.closeDeleteModal        = closeDeleteModal;

        vm.edit                   = edit;
        vm.create                 = create;
        vm.deletee                = deletee;

        activate();

        function activate() {

            $ionicModal.fromTemplateUrl('app/category/category.delete.modal.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.deleteModal = modal;
            });

            $ionicModal.fromTemplateUrl('app/category/category.new.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.createModal = modal;
            });

            $ionicModal.fromTemplateUrl('app/category/category.edit.template.html', {
                scope: $scope,
                animation: 'slide-in-up'
            }).then(function(modal) {
                $scope.editModal = modal;
            });

            $scope.$on('$destroy', function() {
                vm.deleteModal.remove();
                vm.createModal.remove();
                vm.editModal.remove();
            });

            var promises = [getCategories()];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('app.home');

            });

        }

        ////////// Controller functions //////////

        function getCategories() {
            var deferrer = $q.defer();

            categoryService.categories().get(function(categories) {

                deferrer.resolve();
                vm.categories   = categories.data;
                vm.pagination = categories.pagination;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function openDeleteModal(categoryId) {

            vm.categoryId = categoryId;
            $scope.deleteModal.show();

        }

        function openCreateModal() {

            $scope.createModal.show();

        }

        function openEditModal(categoryId) {

            categoryService.categories().get({id: categoryId}, function(category) {

                vm.category   = category.data;
                vm.categoryId = categoryId;
                $scope.editModal.show();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

        function closeEditModal() {

            $scope.editModal.hide();
            vm.category   = {};
            vm.categoryId = '';
        }

        function closeCreateModal() {

            $scope.createModal.hide();
            vm.category   = {};
            vm.categoryId = '';

        }

        function closeDeleteModal() {

            $scope.deleteModal.hide();
            vm.categoryId = '';

        }

        function edit() {

            var data = {
                "icon": vm.category.icon,
                "name": vm.category.name
            };

            categoryService.categories().update({id: vm.categoryId}, data, function(category) {

                logger.success('Category successfully modified!');
                $state.go('app.category', {}, {reload: true});
                closeEditModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });
        }

        function create() {

            categoryService.categories().save(vm.category, function(category) {

                logger.success('Category successfully created!');
                $state.go('app.category', {}, {reload: true});
                closeCreateModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

        function deletee() {

            categoryService.categories().remove({id: vm.categoryId}, function(category) {

                logger.success('Category successfully removed!');
                $state.go('app.category', {}, {reload: true});
                closeDeleteModal();

            }, function(error) {

                logger.error(error.data.data.message);

            });

        }

    }
})();