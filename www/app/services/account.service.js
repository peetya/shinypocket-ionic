(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('accountService', accountService);

    function accountService($resource) {

        return {
            accounts: serviceAccounts
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceAccounts() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/accounts/:id', { id: '@id' }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    }

})();