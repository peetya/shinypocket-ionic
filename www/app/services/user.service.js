(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('userService', userService);

    function userService($resource) {

        return {
            users: serviceUsers
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceUsers() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/users/me', {}, {
                update: {
                    method: 'PUT'
                }
            });
        }
    }

})();