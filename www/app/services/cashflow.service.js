(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('cashflowService', cashflowService);

    function cashflowService($resource) {

        return {
            cashflows: serviceCashflows
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceCashflows() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/cashflows/:id', { id: '@id' }, {
                update: {
                    method: 'PUT'
                }
            });
        }
    }

})();