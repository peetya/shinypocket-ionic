(function () {
    'use strict';

    angular
        .module('shinypocket')
        .factory('reportService', reportService);

    function reportService($resource) {

        return {
            reports: serviceReports,
            charts: serviceCharts
        };

        //////////////////////////
        // Services
        //////////////////////////
        function serviceReports() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/reports/:year/:month', { year: 'year', month: '@month' });
        }

        function serviceCharts() {
            return $resource('http://shinypocket.mybluemix.net/api/v1/charts/:type/:year/:month', { type: 'type', year: '@year', month: '@month'});
        }
    }

})();