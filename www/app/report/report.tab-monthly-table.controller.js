(function() {
    'use strict';

    angular
        .module('shinypocket.report')
        .controller('ReportMonthlyTableController', ReportMonthlyTableController);

    ReportMonthlyTableController.$inject = ['$q', '$stateParams', 'logger', 'categoryService', 'accountService', 'reportService'];

    function ReportMonthlyTableController($q, $stateParams, logger, categoryService, accountService, reportService) {

        var vm = this;

        vm.accountFilter  = null;
        vm.categoryFilter = null;

        vm.report     = {};
        vm.pagination = {};
        vm.accounts   = {};
        vm.categories = {};
        vm.sum        = {
            income:  0,
            expense: 0,
            total:   0
        };
        vm.details    = {
            year: parseInt($stateParams.year),
            month: parseInt($stateParams.month),
            nextYear: 0,
            prevYear: 0,
            nextMonth: 0,
            prevMonth: 0
        };

        activate();

        function activate() {

            vm.details.nextMonth = (vm.details.month + 1 > 12 ? 1 : vm.details.month + 1);
            vm.details.prevMonth = (vm.details.month - 1 == 0 ? 12 : vm.details.month - 1);

            vm.details.nextYear = (vm.details.nextMonth - vm.details.month != 1 ? vm.details.year + 1 : vm.details.year);
            vm.details.prevYear = (vm.details.month - vm.details.prevMonth != 1 ? vm.details.year - 1 : vm.details.year);

            var promises = [getCategories(), getAccounts(), getReport(), getReportWithoutPagination()];

            return $q.all(promises).then(function() {

                if ($stateParams.account) {
                    for (var i = 0; i < vm.accounts.length; i++) {
                        if (vm.accounts[i]._id == $stateParams.account) {
                            vm.accountFilter = vm.accounts[i].name;
                        }
                    }
                }

                if ($stateParams.category) {
                    for (var i = 0; i < vm.categories.length; i++) {
                        if (vm.categories[i]._id == $stateParams.category) {
                            vm.categoryFilter = vm.categories[i].name;
                        }
                    }
                }

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////

        function getCategories() {
            var deferrer = $q.defer();

            categoryService.categories().get(function(categories) {

                deferrer.resolve();
                vm.categories = categories.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function getAccounts() {
            var deferrer = $q.defer();

            accountService.accounts().get(function(accounts) {

                deferrer.resolve();
                vm.accounts = accounts.data;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function getReport() {
            var deferrer = $q.defer();

            reportService.reports().get({ year: $stateParams.year, month: $stateParams.month, account: $stateParams.account, category: $stateParams.category, page: $stateParams.page, limit: 10 }, function(report) {

                deferrer.resolve();
                vm.report = report.data;
                console.log(vm.report);
                vm.pagination = report.pagination;

            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }

        function getReportWithoutPagination() {
            var deferrer = $q.defer();

            reportService.reports().get({ year: $stateParams.year, month: $stateParams.month, account: $stateParams.account, category: $stateParams.category }, function(report) {

                deferrer.resolve();

                for(var i = 0; i < report.data.length; i++) {
                    if(report.data[i].type == 'income') {

                        vm.sum.income = vm.sum.income + report.data[i].amount;
                        vm.sum.total = vm.sum.total + report.data[i].amount;

                    } else {

                        vm.sum.expense = vm.sum.expense + report.data[i].amount;
                        vm.sum.total = vm.sum.total - report.data[i].amount;

                    }
                }


            }, function(error) {

                deferrer.reject();
                logger.error(error.data.data.message);

            });

            return deferrer.promise;
        }
    }
})();