(function() {
    'use strict';

    angular
        .module('shinypocket.report')
        .controller('ReportController', ReportController);

    ReportController.$inject = ['$q', '$stateParams', 'logger', 'categoryService', 'accountService', 'reportService'];

    function ReportController($q, $stateParams, logger, categoryService, accountService, reportService) {

        var vm = this;

        activate();

        function activate() {

            var promises = [];

            return $q.all(promises).then(function() {

                // do nothing...

            }, function() {

                //TODO: redirect to 500
                $state.go('home');

            });

        }

        ////////// Controller functions //////////



    }
})();