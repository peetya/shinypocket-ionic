(function() {
    'use strict';

    angular
        .module('shinypocket.report')
        .config(stateConfig);

    stateConfig.$inject = ['$stateProvider'];

    function stateConfig($stateProvider) {

        $stateProvider
            .state('app.yearly', {
                url: "/yearly/:year",
                authenticate: true,
                abstract: true,
                views: {
                    'menuContent': { templateUrl: "app/report/report.yearly.template.html" }
                }
            })

            .state('app.yearly.tab-yearly-table', {
                url: "/table?page",
                params: {
                    page: '1',
                    squash: true
                },
                views: {
                    'tab-yearly-table': {
                        templateUrl: "app/report/report.tab-yearly-table.template.html",
                        controller: "ReportYearlyTableController as vm"
                    }
                }
            })

            .state('app.yearly.tab-yearly-pie', {
                url: "/pie",
                views: {
                    'tab-yearly-pie': {
                        templateUrl: "app/report/report.tab-yearly-pie.template.html",
                        controller: "ReportYearlyChartController as vm"
                    }
                }
            })

            .state('app.monthly', {
                url: "/monthly/:year/:month",
                authenticate: true,
                abstract: true,
                views: {
                    'menuContent': { templateUrl: "app/report/report.monthly.template.html" }
                }
            })

            .state('app.monthly.tab-monthly-table', {
                url: "/table?page",
                params: {
                    page: '1',
                    squash: true
                },
                views: {
                    'tab-monthly-table': {
                        templateUrl: "app/report/report.tab-monthly-table.template.html",
                        controller: "ReportMonthlyTableController as vm"
                    }
                }
            })

            .state('app.monthly.tab-monthly-pie', {
                url: "/pie",
                views: {
                    'tab-monthly-pie': {
                        templateUrl: "app/report/report.tab-monthly-pie.template.html",
                        controller: "ReportMonthlyChartController as vm"
                    }
                }
            })

    }

})();