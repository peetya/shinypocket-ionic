(function() {
    'use strict';

    angular
        .module('shinypocket.error')
        .controller('ErrorController', ErrorController);

    ErrorController.$inject = ['logger'];

    function ErrorController() {

        var vm = this;

        vm.error = {
            title: '404',
            content: 'Page not found'
        };
    }
})();